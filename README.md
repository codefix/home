# No Place Like ${HOME} #

## home-sync ##

Synchronises a file set with $HOME. The files in [hs.example](/hs.example/) are provided as a sample, based on the author's preferred configuration. Run `home-sync --help` for full usage details.

## kungfu ##

KungFu  was  written  to aid the author, as well as anyone else who finds this sort of thing useful, to easily define shell aliases to set up project environments. The idea is to be able to open related files, folders, and terminal window with just one command. See `kungfu --help` for full usage details.