#!/bin/zsh
die() { #{{{
    print $@
    bye 1
} #}}}
usage() { #{{{
    { # default to $PAGER if pandoc isn't in $PATH.
        if [[ -z $(whence pandoc) ]] {
            : ${PAGER:=less}
            ${PAGER}
        } else {
            pandoc --normalize -RSst man | man -l -
        }
    } <<-EOF
	% HOME-SYNC(1) home-sync user manual
	% Garrison Hoffman <garrison.hoffman@gmail.com>
	
	<!--
	        Install pandoc to view this as a man page.
	-->
	
	# NAME
	
	home-sync --- ${HOME} synchronization.
	
	# SYNOPSIS
	
	home-sync [*OPTIONS*] -- [*COMMAND OPTIONS*]
	
	# DESCRIPTION
	
	This program will compare the differences (\--diff), synchronize (\--sync),
	or interactively merge (\--merge) files from the current directory (or PATH
	supplied with \--git-dir) to \$HOME (${HOME}).
	
	Options and other input following '\--' will be passed as arguments to the
	diff, merge, or sync command.
	
	# MAIN OPTIONS
	
	\--debug
	:   Prints debugging info & inhibits file changes.
	-e, \--exclude=*PATH*
	:   Override default exclude file.
	    Default: *\$GITDIR/hs.exclude*
	
	    (Current: ${EXCLUDE})
	-g, \--git-dir=*PATH*
	:   Use versioned files in *PATH* rather than autodetected dir.
	    If no command line path is specified, the following default
	    values will be checked in descending order of preference:
	
	        * \${HOME_SYNC} environment variable.
	        * ~/.config/home-sync
	        * <program installation path>/hs.example
	
	    (Current: ${GITDIR})
	
	-h, \--help
	:   Show usage, then exit.
	-v, \--verbose
	:   Increase verbosity by 1.
	\--verbose=*N*
	:   Set verbosity to *N*.
	
	# DIFF OPTIONS
	-d, \--diff[=*CMD*]
	:   Show changes between file sets; the long form allows
	    *CMD* to override the default command.
	
	# MERGE OPTIONS
	-m, \--merge[=*CMD*]
	:   Merge changes between file sets; the long form allows
	    *CMD* to override the default command.
	
	# SYNC OPTIONS
	-i, \--import
	:   Import file changes from ${HOME}.
	-s, \--sync[=*CMD*]
	:   Push files to ${HOME}; the long form allows
	    *CMD* to override the default command.
	-u, \--update
	:   Add \--update to rsync options.
	
	EOF
    exit
} #}}}
process_options() { #{{{
    PARAMS=$(getopt -o de:g:himsuvx \
        --long debug,diff::,exclude:,git-dir:,help \
        --long import,merge::,sync::,update,verbose:: \
        -n $0 -- "$@")
    # Bail unless getopt returned 0
    [[ $? = 0 ]] || die Terminating...

    eval set -- ${PARAMS}
    while true; do
        case ${1} in
            -h|--help)    usage; shift;;
            -u|--update)  RSOPTS+='--update'; shift;;
            -e|--exclude) EXCLUDE=${2}; shift 2;;
            -g|--git-dir) GITDIR=${2};  shift 2;;
            # Core options (import/diff/merge/sync) # {{{
            -i|--import)  MODE='IMPORT'; shift;;

            --diff)       MODE='DIFF';  [[ -n ${2} ]] && DIFFER=${2}; shift 2;;
            -d)           MODE='DIFF'; shift;;

            --merge)      MODE='MERGE'; [[ -n ${2} ]] && MERGER=${2}; shift 2;;
            -m)           MODE='MERGE'; shift;;

            --sync)       MODE='RSYNC'; [[ -n ${2} ]] && SYNCER=${2}; shift 2;;
            -s)           MODE='RSYNC'; shift;;
            # }}}
            # Debug/Verbose #{{{
            --debug)        DEBUG=true; MODE='DEBUG'
                            RSOPTS+='--dry-run'
                            MGOPTS+='-R'
                            shift;;

            -v)             VERBOSE=$(( ${VERBOSE} + 1 )); shift;;
            --verbose)      case ${2} in
                            "") VERBOSE=$(( ${VERBOSE} + 1 ));    shift 2;;
                            *) VERBOSE=$(( ${VERBOSE} + ${2} )); shift 2;;
                            esac;;
            #}}}
            --) shift; break;;
            *)  usage;;
        esac
    done
    # Post options fixup #{{{
    XOPTS="$@"
    if [[ ${VERBOSE} -gt 0 ]]; then
        DFOPTS=(-u --new-file)
        for i in {1..${VERBOSE}}; do
            RSOPTS+='--verbose'
        done
    fi
    [[ ${MODE} =~ 'IMPORT' ]] || RSOPTS+="--exclude-from=${EXCLUDE:a}"
    #}}}
} #}}}
debug() { #{{{
    print  "   MODE: ${MODE}"
    print  "   HOME: ${HOME}"
    print  " GITDIR: ${GITDIR}"
    print  "EXCLUDE: ${EXCLUDE}"
    print  "VERBOSE: ${VERBOSE}"
    printf "  MERGE: %s %s %s %s\n" ${MERGER} ${MGOPTS}
    printf "   DIFF: %s %s %s %s\n" ${DIFFER} ${DFOPTS}
    printf "   SYNC: %s %s %s %s\n" ${SYNCER} ${RSOPTS/${~GITDIR}/\$GITDIR}
    printf "  XOPTS: %s\n" ${XOPTS}
    [[ ${MODE} =~ 'DEBUG' ]] && return
    repeat 72 print -n '#'; print ''
} #}}}
ckperm() { #{{{
    typeset P
    P=$(stat -c%a ${HOME})
    ((${P} > ${CKMODE} )) || return
    print "Mode ${P} on ${HOME}; Setting ${CKMODE}."
    chmod ${CKMODE} ${HOME}
} #}}}
hsync() { #{{{
    [[ -r ${EXCLUDE} ]] || die "Can't find '${EXCLUDE}'."
    ${SYNCER} ${RSOPTS} ${XOPTS} "${GITDIR}/" ${HOME}
    ckperm
} #}}}
checkdiff() { #{{{
    diff --brief --new-file ${1} ${2}
} #}}}
manifest() { #{{{
    [[ -r ${EXCLUDE} ]] || die "Can't find '${EXCLUDE}'."
    FINDOPTS=(-type f)
    for DIR ($(cat ${EXCLUDE})) { FINDOPTS+=(! -path "./${DIR}*"); }
} #}}}
compare() { #{{{
    cd ${GITDIR} && manifest
    for F ($(find . ${FINDOPTS} -printf "%P\n")) {
        local DST="${HOME}/${F}"
        if [[ -n $(checkdiff ${DST} ${F}) ]] {
            if (( ${VERBOSE} > 0 )) {
                print ''
                ${DIFFER} ${DFOPTS} ${XOPTS} "${HOME}/${F}" ${F}
                print ''
            } else {
                print "   DIFFS: ${DST}"
            }
        } else {
            (( ${VERBOSE} > 1 )) && print "NO DIFFS: ${F}"
        }
    }
} #}}}
merge() { #{{{
    cd ${GITDIR} && manifest
    for F ($(find . ${FINDOPTS} -printf "%P\n")) {
        local DST="${HOME}/${F}"
        if [[ ! -f ${DST} || -n $(checkdiff ${F} ${DST}) ]]; then
            ${MERGER} ${MGOPTS} ${XOPTS} "${F}" "${HOME}/${F}"
        fi
    }
} #}}}
import() { #{{{
    cd ${GITDIR} && manifest
    for F ($(find . ${FINDOPTS} -printf "%P\n")) {
        SRC="${HOME}/${F}"
        if [[ -f ${SRC} && -n $(checkdiff ${F} ${SRC}) ]]; {
            cd ~/ && ${SYNCER} ${RSOPTS} ${XOPTS} ${F} "${GITDIR}/${F}"
            cd ${GITDIR}
        }
    }
} #}}}
local GITDIR DIFFER MERGER SYNCER
local EXCLUDE VERBOSE DEBUG MODE CKMODE
local -a DFOPTS MGOPTS RSOPTS
# Default values #{{{
DIFFER=$(which colordiff)
MERGER=$(which gvimdiff)
SYNCER=$(which rsync)
DFOPTS=(--brief --new-file)
MGOPTS=(--nofork)
RSOPTS=(-rlpD)
CKMODE=750
VERBOSE=0
DEBUG=false
UPDATE=false
GITDIR=${0:a:h}/hs.example
[[ -d ~/.config/home-sync ]] && GITDIR=~/.config/home-sync
[[ -n ${HOME_SYNC} && -d ${HOME_SYNC} ]] && GITDIR=${HOME_SYNC}
[[ -r ${GITDIR}/hs.exclude ]] && EXCLUDE=${GITDIR}/hs.exclude
#}}}
process_options $@
${DEBUG} && debug;
[[ -d ${GITDIR} ]] || die "Directory not found: ${GITDIR}"
case ${MODE} in
     DIFF) compare;;
    MERGE) merge;;
    RSYNC) hsync;;
   IMPORT) import;;
    DEBUG) ;;
        *) usage;;
esac
