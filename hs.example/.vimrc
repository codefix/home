" .vimrc
" Load Pathogen "{{{
" see https://bugs.launchpad.net/ubuntu/+source/vim/+bug/572627
if filereadable($HOME . '/.vim/autoload/pathogen.vim')
    call pathogen#infect()
    call pathogen#runtime_append_all_bundles()
endif
"}}}
syntax on
filetype plugin indent on
" Default settings "{{{
set encoding=utf-8 nobomb    " BOM often causes trouble
set nocompatible             " Do not stay vi compatible
set tabstop=4
set expandtab
set shiftwidth=4
set shiftround
set lbr
set spl=en_us
set incsearch
set hlsearch
set foldmethod=marker
set autoindent
"}}}
if version >= 703  "{{{
    set colorcolumn=80
    set cursorline
endif
"}}}
" Register <-> Clipboard interaction  "{{{
if has("xterm_clipboard")
    " Use the visual selection buffer (XA_PRIMARY)
    " as the default register (viz. "*)
    set clipboard=unnamed
elseif has("unnamedplus")
    " Use the cut & paste buffer (XA_CLIPBOARD)
    " as the default register (viz. "+)
    set clipboard=unnamedplus
endif
"}}}
if &diff  "{{{
    set noexpandtab
endif
"}}}
" Filetype settings "{{{
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
let g:Tex_TEXINPUTS = '~/texmf/LaTeX/**,./**'
autocmd FileType c,cpp,cs set fdm=marker fmr={,}
autocmd FileType mail set textwidth=69
"}}}
" JellyX colorscheme settings "{{{
if filereadable($HOME . '/.vim/colors/jellyx.vim')
    set background=dark
    let g:jellyx_show_whitespace = 1 " highlight tabs & trailing spaces
    colorscheme jellyx
endif
"}}}
if has("gui_running")  "{{{
    set t_Co=256
    set gfn=Liberation\ Mono\ 12
    set lines=50
    set mousemodel=popup
endif
"}}}
" Perl settings "{{{
" comment/uncomment blocks of code (in vmode)
vmap <A-C> :s/^#//gi<CR>
vmap <A-c> :s/^/#/gi<CR>

" my perl includes pod
let perl_include_pod = 1
" syntax color complex things like @{${"foo"}}
let perl_extended_vars = 1

" Tidy selected lines (or entire file) with _t:
nnoremap <silent> _t :%!perltidy -q<Enter>
vnoremap <silent> _t :!perltidy -q<Enter>
""}}}
" XML settings "{{{
let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax
"}}}
" Custom mappings "{{{
" combine searches (nN#*) with window centering (zz)
nmap n nzz
nmap N Nzz
nmap * *zz
nmap # #zz
"nmap g* g*zz
"nmap g# g#zz
nmap z\ za
nmap // :noh<return>
map <F2> :set spell! spell?<CR>
nmap <F3> i<C-R>=strftime("%Y-%m-%d %a %I:%M:%S %p %Z")<CR>
imap <F3> <C-R>=strftime("%a %d %b %I:%M:%S %p")<CR>
if version >= 703
    map <F8> :set rnu! rnu?<CR>
endif
map <F12> :set wrap! wrap?<CR>
vnoremap ;rv c<C-O>:set revins<CR><C-R>"<Esc>:set norevins<CR>
map <A-o> G :1,$+1diffget<CR>
map <A-p> G :1,$diffput<CR>
command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis | wincmd p | diffthis
"}}}
" Custom digraphs "{{{
dig -. 8230 "U+2026=…  HORIZONTAL ELLIPSIS
"}}}
" Custom Helpful() text "{{{
fu! Helpful()
echo " "
echo "  Ctrl+k      Enter digraph. (:dig[raph], :help digraph-table)"
echo "  Ctrl+Vx     Enter Unicode hex values <= FF"
echo "  Ctrl+Vu     Enter Unicode hex values <= FFFF"
echo "  Ctrl+VU     Enter Unicode hex values <= 7FFFFFFF"
echo "  ga          Print character's :as[cii] value in dec, hex, oct."
echo " "
echo "  F2          Toggle :set [no]spell."
echo "  z=          Spelling suggestions."
echo "  ]s, [s      Next, previous mispelling."
echo "  zg, zG      Add (good) word to dictionary, temp. dictionary."
echo "  zw, zW      Add (wrong) word to dictionary, temp. dictionary."
echo "  zu[gGwW]    Remove (good|wrong) word from dictionary, temp. dictionary."
echo " "
echo "  F3          Insert current date & time."
echo "  F7          This help."
echo "  F8, F12     Toggle line numbering, line wrap."
echo " "
echo "  zf, zf%     Fold selected text, to closing brace."
echo "  z\\ (za)     Toggle this fold open/close."
echo "  zo, zO      Open this fold, open recursively."
echo "  zm, zr      Increase fold level, Decrease fold level."
echo "  zM, zR      Close all folds, Open all folds."
echo "  zj, zk      Jump to the next, previous fold."
echo "  [z, ]z      Move to this fold's beginning, end."
echo "  zz, z↵, z-  Scroll to center, top, bottom. (:help scroll-cursor)"
echo " "
echo "  Alt+o       Obtain (get) all diffs."
echo "  Alt+p       Put all diffs."
echo " "
echo "  ^wn, ^wv    Split horizontally, vertically."
echo "  ^wr, ^wR    Rotate window down, up."
echo "  ^w<arrow>   Move between windows."
echo "  ^w=         Size windows equally."
echo " "
echo "  Also see…"
echo " "
echo " :help modeline"
endf
map <F7> :exe Helpful()<CR>
"}}}
" Jump to cursor on edit. "{{{
" http://vim.sourceforge.net/tips/tip.php?tip_id=80
augroup JumpCursorOnEdit
  au!
  autocmd BufReadPost *
    \ if expand("<afile>:p:h") !=? $TEMP |
    \   if line("'\"") > 1 && line("'\"") <= line("$") |
    \     let JumpCursorOnEdit_foo = line("'\"") |
    \     let b:doopenfold = 1 |
    \     if (foldlevel(JumpCursorOnEdit_foo) > foldlevel(JumpCursorOnEdit_foo - 1)) |
    \        let JumpCursorOnEdit_foo = JumpCursorOnEdit_foo - 1 |
    \        let b:doopenfold = 2 |
    \     endif |
    \     exe JumpCursorOnEdit_foo |
    \   endif |
    \ endif
  " Need to postpone using "zv" until after reading the modelines.
  autocmd BufWinEnter *
    \ if exists("b:doopenfold") |
    \   exe "normal zv" |
    \   if(b:doopenfold > 1) |
    \       exe  "+".1 |
    \   endif |
    \   unlet b:doopenfold |
    \ endif
augroup END
"}}}
