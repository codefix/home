#!/usr/bin/zsh
# grue prompt theme
prompt_grue_help () {  # {{{
  cat <<'EOF'
Grue supports color schemes and git status information. Dark colors are
recommended, such as Gnome's Tango palette.

You can invoke it thusly:

  prompt grue [<User-color> [<PWD1-color> [<PWD2-color>]]]

  User-color  Username & host: "user@host".         (default: blue)
  PWD1-color  Present working directory.            (default: cyan)
  PWD2-color  Present working directory when the    (default: magenta)
              prompt is split over two lines.

When $PWD is a git repository, grue displays the current branch as well as
unicode characters indicating when a branch has uncommitted changes (⚫), or
when a local branch is ahead (▲), behind (▼), or otherwise diverged (⧫) from
an upstream branch.

Grue colors the branch name & branch status as per PWD2-color & PWD1-color
when the current branch is 'master', for all other branches the colors are
the other way around.
EOF
}
# }}}
prompt_grue_setup () {  # {{{
    grue_color1=${1:-'blue'}
    grue_color2=${2:-'cyan'}
    grue_color3=${3:-'magenta'}

    base_prompt="%F{$grue_color1}%n@%m%f "
    post_prompt="%f"

    base_prompt_no_color=$(echo "$base_prompt" | perl -pe "s/%(F\{.*?}|f)//g")
    post_prompt_no_color=$(echo "$post_prompt" | perl -pe "s/%(F\{.*?}|f)//g")

    add-zsh-hook precmd prompt_grue_precmd
    add-zsh-hook preexec prompt_grue_preexec
    add-zsh-hook chpwd prompt_grue_git_vars
}
# }}}
prompt_grue_preexec () {  # {{{
    case "$1" in
        git*) __EXECUTED_GIT_COMMAND=1 ;;
    esac
}
# }}}
prompt_grue_precmd () {  # {{{
  setopt noxtrace localoptions
  local base_prompt_expanded_no_color base_prompt_etc
  local prompt_length space_left

  if [ -n ${__EXECUTED_GIT_COMMAND} ]; then
    prompt_grue_git_vars
    unset __EXECUTED_GIT_COMMAND
  fi

  base_prompt_expanded_no_color=$(print -P "$base_prompt_no_color")
  base_prompt_etc=$(print -P "$base_prompt%(4~|...|)%3~")
  prompt_length=${#base_prompt_etc}
  if [[ $prompt_length -lt 40 ]]; then
    path_prompt="%F{$grue_color2}%(4~|...|)%3~%F{white}"
  else
    space_left=$(( $COLUMNS - $#base_prompt_expanded_no_color - 2 ))
    path_prompt="%F{$grue_color2}%${space_left}<...<%~$prompt_newline%F{white}"
  fi


  PS1="$base_prompt$path_prompt $(prompt_grue_git_info)%# $post_prompt"
  PS2="$base_prompt$path_prompt $(prompt_grue_git_info)%_>  $post_prompt"
  PS3="$base_prompt$path_prompt $(prompt_grue_git_info)?# $post_prompt"
}
# }}}
prompt_grue_git_vars () {  # {{{
    unset __CURRENT_GIT_BRANCH
    unset __CURRENT_GIT_BRANCH_STATUS
    unset __CURRENT_GIT_BRANCH_IS_DIRTY

    local st="$(git status 2>/dev/null)"
    if [[ -n "$st" ]]; then
        local -a arr
        arr=(${(f)st})

        if [[ $arr[1] =~ 'Not currently on any branch.' ]]; then
            __CURRENT_GIT_BRANCH='no-branch'
        else
            __CURRENT_GIT_BRANCH="${arr[1][(w)4]}";
        fi

        if [[ $arr[2] =~ 'Your branch is (ahead|diverged|behind)' ]]; then
            __CURRENT_GIT_BRANCH_STATUS=${match}
        fi

        if [[ ! $st =~ 'nothing to commit' ]]; then
            __CURRENT_GIT_BRANCH_IS_DIRTY='1'
        fi
    fi
}
# }}}
prompt_grue_git_info () {  # {{{
    if [ -n "$__CURRENT_GIT_BRANCH" ]; then
        local git_color1 # branch name
        local git_color2 # branch status

        if [[ "$__CURRENT_GIT_BRANCH" != "master" ]]; then
            git_color1=$grue_color3
            git_color2=$grue_color2
        else
            git_color1=$grue_color2
            git_color2=$grue_color3
        fi

        local s="%F{$git_color1}$__CURRENT_GIT_BRANCH"
        [[ -n "$__CURRENT_GIT_BRANCH_IS_DIRTY" ]] && s+="%B%F{$git_color2}⌁%b"
        case "$__CURRENT_GIT_BRANCH_STATUS" in
            ahead) s+="%F{$git_color2}⁺" ;;
            diverged) s+="%F{$git_color2}±" ;;
            behind) s+="%F{$git_color2}₋" ;;
        esac
        s+="%F{$grue_color1}"

        case "$__CURRENT_GIT_BRANCH" in
            master) printf "%s{%s}%s" %F{$grue_color1} $s %F{white} ;;
              dev*) printf "%s[%s]%s" %F{$grue_color1} $s %F{white} ;;
                 *) printf "%s(%s)%s" %F{$grue_color1} $s %F{white} ;;
        esac
    fi
}
# }}}
prompt_grue_setup "$@"
