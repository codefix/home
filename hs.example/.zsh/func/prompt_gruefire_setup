# Fire prompt theme from bashprompt
# Inspired by Raster (Carsten Haitzler of Red Hat Advanced Development Labs)
# Created by BadlandZ
# Changed by Spidey 08/06
# Converted to zsh prompt theme by <adam@spiers.net>
# Changed again by 02/12 <garrison@codefix.net>
prompt_gruefire_help () { # {{{
  cat <<EOH
This prompt supports color schemes. Use of a font with UTF-8 support is
recommended. GrueFire can be invoked thusly:

  prompt gruefire [<fire1> [<fire2> [<cwd> [<date> [<fire3>]]]]]

Where the parameters are:

 [Parameter] [Default]  [Description]
    fire1     red        Gradient lead-in.
    fire2     none*      Gradient midsection.
      cwd    <fire1>     Current working directory.
     date    <cwd>       Date text.
    fire3    <fire1>     Gradient lead-out.

*When no parameters are supplied, the defaults are red, yellow, yellow, black.

EOH
} # }}}
prompt_gruefire_setup () { # {{{
  setopt rematch_pcre
  local fire1 fire2 fire3 cwd date
  [[ -z $@ ]] && {
    fire2='yellow'
    cwd='yellow'
    date='black'
  }
  fire1=${1:-'red'}
  : ${fire2:=${2:-'none'}}
  : ${cwd:=${3:-$fire1}}
  : ${date:=${4:-$cwd}}
  fire3=${5:-$fire1}

  local -a schars
  autoload -Uz prompt_special_chars
  prompt_special_chars

  local B0='%b' B1='%b' B2='%b'
  [[ ${date} == ${fire2} ]] && B0="%B"
  [[ ${date} =~ '(bl?$|bla)' && ${fire2} == 'none' ]] && B0="%B"
  [[ ${fire1} =~ '(bl?$|bla)' ]] && B1="%B"
  [[ ${cwd} =~ '(bl?$|bla)' ]] && B2="%B"
  local GRAD1="%{$schars[333]$schars[262]$schars[261]$schars[260]%}"
  local GRAD2="%{$schars[260]$schars[261]$schars[262]$schars[333]%}"
  local COLOR0="${B1}%F{$fire1}%K{none}"
  local COLOR1="${B1}%F{$fire1}%K{$fire2}"
  local COLOR2="${B0}%F{$date}%K{$fire2}"
  local COLOR3="${B1}%F{$fire3}%K{$fire2}"
  local COLOR4="${B1}%F{$fire3}%K{none}"
  local COLOR5="${B2}%F{$cwd}%K{none}"
  local GRAD0="%b%f%k"

  PS1=$COLOR0$GRAD2$COLOR1$GRAD1$COLOR2' %D{%a %b %d} %D{%I:%M:%S%P} '$COLOR3$GRAD2$COLOR4$GRAD1$prompt_newline$COLOR5'%~/'$GRAD0' '
  PS2=$COLOR1$GRAD1$COLOR3$GRAD2$COLOR4$GRAD1$COLOR5'>'$GRAD0' '

  prompt_opts=(cr subst percent)
} # }}}
prompt_gruefire_preview () { # {{{
  if (( ! $#* )); then
    prompt_preview_theme gruefire
    print
    prompt_preview_theme gruefire red yellow
  else
    prompt_preview_theme gruefire "$@"
  fi
} # }}}
prompt_gruefire_setup "$@"
