# zshrc — interactive shell
# zshoptions #{{{
# man zshoptions…
setopt autocd autopushd cdablevars pushdignoredups
setopt extendedglob notify complete_in_word
setopt hist_ignore_all_dups hist_ignore_dups hist_ignore_space
bindkey -e
HISTFILE=~/.zsh_history
HISTSIZE=1024
SAVEHIST=1024
autoload -U promptinit && promptinit
# prompt -s not implemented as of 2012-01
if [[ $EUID == 0 ]] {
    prompt grue red yellow
} elif [[ -n ${SSH_CLIENT} ]] {
    prompt grue green cyan blue
} else {
    prompt grue
}
#}}}
# The following lines were added by compinstall  # {{{

zstyle ':completion:*' completer _oldlist _expand _complete _ignored _match _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' max-errors 2 numeric
zstyle ':completion:*' menu select=3
zstyle ':completion:*' original true
zstyle ':completion:*' prompt '%e errors: '
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' substitute 1
zstyle :compinstall filename '/home/garrison/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# }}}
# Remove uninteresting users # {{{
zstyle ':completion:*:*:*:users' ignored-patterns \
   adm alias apache at bin cron cyrus daemon ftp games gdm guest \
   haldaemon halt mail man messagebus mysql named news nobody nut \
   lp operator portage postfix postgres postmaster qmaild qmaill \
   qmailp qmailq qmailr qmails shutdown smmsp squid sshd sync \
   uucp vpopmail xfs \
   'avahi*' backup couchdb festival gnats hplip irc kernoops libuuid \
   libvirt-qemu list proxy pulse rtkit saned speech-dispatcher sys \
   stunnel4 usbmux www-data
# }}}
# Remove uninteresting hosts # {{{
zstyle ':completion:*:*:*:hosts-host' ignored-patterns \
   '*.*' loopback localhost
zstyle ':completion:*:*:*:hosts-domain' ignored-patterns \
   '<->.<->.<->.<->' '^*.*' '*@*'
zstyle ':completion:*:*:*:hosts-ipaddr' ignored-patterns \
   '^<->.<->.<->.<->' '127.0.0.<->'
zstyle -e ':completion:*:(ssh|scp):*' hosts 'reply=(
   ${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) \
   /dev/null)"}%%[# ]*}//,/ }
   )'
# }}}
[[ -r ~/.gvm/scripts/gvm ]] && source ~/.gvm/scripts/gvm
[[ -r ~/.zlocal ]] && source ~/.zlocal
[[ -r ~/.zshrc_local ]] && source ~/.zshrc_local
