# ~/.zshenv - used by all shells.
setopt dvorak rematch_pcre
# Setup PATH, FPATH, GOPATH #{{{
typeset -U PATH FPATH # Uniquify
for P (/usr/local/bin ~/Code/GoogleCode/depot_tools) {
    [[ -d ${P} ]] && path+=${P}
}
for dir (func comp) {
    if [[ -d ~/.zsh/${dir} ]] {
        fpath+=~/.zsh/${dir}
    } elif [[ -d ${dir} ]] {
        fpath+=${dir}
    }
}
export PATH FPATH
#}}}
autoload bulk-alias set-icons set-icon linkdirs
# X|!X #{{{
if [[ -n $(whence X) && -n $(pgrep -lf $(whence X)) ]] {
    export XMODIFIERS='@im=XIM'
    export GTK_IM_MODULE="xim"
    export QT_IM_MODULE="xim"
    SSH_ASKPASS=$(whence ssh-askpass)
    [[ -x ${SSH_ASKPASS} ]] && export SSH_ASKPASS
    export EDITOR='vim -fpg'
} else {
    export EDITOR='vim'
} #}}}
# ${CODE} Aliases #{{{
if [[ -d ~/Code ]] {
    CODE=~/Code; export CODE
    # bulk-alias aliases #{{{
    if [[ -d ${CODE}/sysadmin/ ]] {
        bulk-alias ${CODE}/sysadmin/* ${CODE}/sysadmin/mail-tools/*
    }
    [[ -d ${CODE}/BSS/bin/  ]] && bulk-alias ${CODE}/BSS/bin/*
    [[ -d ${CODE}/cloudkit/ ]] && bulk-alias ${CODE}/cloudkit/*
    [[ -d ${CODE}/oddments/ ]] && bulk-alias ${CODE}/oddments/*
    [[ -d ${CODE}/pub/      ]] && bulk-alias ${CODE}/pub/[pt]*/*
    [[ -d ${CODE}/scripts/  ]] && bulk-alias ${CODE}/scripts/*/*
    [[ -d ${CODE}/home/     ]] && bulk-alias ${CODE}/home/*
    #}}}
} #}}}
# Login shells 1ˢᵗ (.zprofile) #{{{
if [[ -o login ]] {
} #}}}
# Interactive shells (.zshrc) #{{{
if [[ -o interactive ]] {
    # Aliases #{{{
    alias gcloud-configs='gcloud config configurations'
    alias dfx='df -h -xtmpfs -xdevtmpfs -xsquashfs'
    alias su='su -sH'       # There's no place like ${HOME}
    alias visudo='sudo visudo -f /etc/sudoers.d/sudoers'
    alias zap='export HISTFILE=dev/null && prompt grue black yellow blue'
    alias rename-lcase='rename "s/(.*)/\L\$1/"'
    alias rename-downpour='rename "s/(.*) \(Unabridged\)  \[File (\d+) of \d+\]/\$2 \$1/"'
    alias rsFancy='rsync -ahu --progress --stats'
    alias rsOnce='rsync -ahu --progress --remove-source-files --stats'
    alias rmdir='rmdir --ignore-fail-on-non-empty'
    alias xterm-title='print -Pn "\e]0;%~\a"'
    alias xterm-title1='print -Pn "\e]0;%1~\a"'
    alias xterm-title2='print -Pn "\e]0;%2~\a"'
    alias whois-xyz='whois -h whois.nic.xyz'
    alias mplayer-id='mplayer -identify -ao null -vo null -nocache -really-quiet -frames 0 2> /dev/null'
    #}}}
    # Kung-Fu Aliases #{{{
    if [[ -f ~/.zsh/${HOST}/kungfu && -x $(whence kungfu) ]] {
        typeset -A DIRS
        DIRS=$(<~/.zsh/${HOST}/kungfu)
        for DIR (${(f)DIRS[@]}) {
            alias ${DIR:t:gs/ /-}="cd '${DIR:A}'; kungfu"
        }
    } #}}}
    # apt-aliases #{{{
    if [[ -x /usr/bin/apt ]] {
        alias aa-uu='sudo apt update && sudo apt upgrade'
    } elif [[ -x /usr/bin/apt-get ]] {
        alias aa-uu='sudo apt-get update && sudo apt-get upgrade'
        alias aa-inst='sudo apt-get install'
        alias aa-purge='sudo apt-get purge'
        alias aa-rm='sudo apt-get remove'
        alias aa-show='aptitude show'
    }
    #}}}
    # Color aliases & ENV vars #{{{
    whence -p colorgcc && alias gcc="colorgcc"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    if [[ -n $(whence -p colordiff) ]]; then
        alias sdiff='sdiff --diff-program=colordiff'
        alias diff="colordiff"
    fi
    export GREP_COLORS='1;32'
    export LESS_TERMCAP_mb=$'\E[01;36m'
    export LESS_TERMCAP_md=$'\E[01;34m'
    export LESS_TERMCAP_me=$'\E[0m'
    export LESS_TERMCAP_se=$'\E[0m'
    export LESS_TERMCAP_so=$'\E[0;40;37m'
    export LESS_TERMCAP_ue=$'\E[0m'
    export LESS_TERMCAP_us=$'\E[01;32m'
    #}}}
    [[ -r ~/.dir_colors ]] && eval $(dircolors ~/.dir_colors)
} #}}}
# Login Shells 2ⁿᵈ (.zlogin) #{{{
if [[ -o login ]] {
} #}}}
# Amazon Cloud Environment Variables #{{{
AWS_CLOUDWATCH_HOME=~/Cloud/CloudWatch
AWS=~/Private/AWS
if [[ -d ${AWS_CLOUDWATCH_HOME} ]] {
    PATH+=:${AWS_CLOUDWATCH_HOME}/bin
    export AWS_CLOUDWATCH_HOME PATH
}
if [[ -d $AWS ]] {
    export EC2_PRIVATE_KEY=$(find $AWS -name pk-\*.pem)
    export EC2_CERT=$(find $AWS -name cert-\*.pem)
    alias aws-instances="ec2-describe-instances|grep -P '(?<=instance)\s+i-\w+'"
} #}}}
if [[ -r /usr/lib/jvm/java-6-openjdk/ ]] { # Got Java? #{{{
    export JAVA_HOME=/usr/lib/jvm/java-6-openjdk/
} #}}}
export TERM=xterm-256color
[[ -r ~/.zshenv_local ]] && source ~/.zshenv_local
