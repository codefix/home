" sudoers filetypes, because modeline is disabled for root by default
au BufRead,BufNewFile *sudoers          set filetype=sudoers
au BufRead,BufNewFile /etc/sudoers.d/*  set filetype=sudoers
