" apache filetypes
au BufRead,BufNewFile *.com.conf,*.net.conf,*.org.conf set filetype=apache
au BufRead,BufNewFile *.ssl.conf,*.web.conf            set filetype=apache
au BufRead,BufNewFile /etc/apache2/sites-*             set filetype=apache
