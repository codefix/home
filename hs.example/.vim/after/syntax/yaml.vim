" ~/.vim/after/syntax/yaml.vim | ~/vimfiles/after/syntax/yaml.vim
" Provides:	Highlighting for ✴U+2734✴ Expansion Tokens
highlight def link utfGRP Label
highlight def link utfSTR Type
match utfGRP /[✴]/
2match utfSTR /[✴]\zs\w*\ze[✴]/
